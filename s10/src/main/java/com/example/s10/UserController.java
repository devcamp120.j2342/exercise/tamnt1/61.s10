package com.example.s10;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.s10.models.Item;
import com.example.s10.models.User;

@RestController
@CrossOrigin
public class UserController {

    @GetMapping("/item")
    public Item getItem() {
        User user = new User(1, "John");
        Item item = new Item(2, "book", user);
        user.addItem(item);
        return item;
    }
}
