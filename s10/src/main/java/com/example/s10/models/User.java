package com.example.s10.models;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {
    public int id;
    public String name;
    @JsonIgnore
    public List<Item> userItems;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
        userItems = new ArrayList<>();
    }

    public void addItem(Item item) {
        this.userItems.add(item);
    }
}
